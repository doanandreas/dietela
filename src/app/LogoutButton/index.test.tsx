import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import LogoutButton from '.';
import { UserContext } from 'provider';

describe('LogoutButton', () => {
  const initialUser = {
    id: null,
    email: '',
    name: '',
    role: null,
  };

  const userContextMock = {
    user: initialUser,
    firstAuthenticated: false,
    isAuthenticated: true,
    isUnpaidClient: false,
    isPaidClient: false,
    isNutritionist: false,
    isAdmin: false,
    isLoading: false,
    isFirstLoading: false,
    signup: jest.fn(),
    login: jest.fn(),
    loginWithGoogle: jest.fn(),
    logout: jest.fn(),
  };

  it('renders correctly', () => {
    render(<LogoutButton />, ROUTES.checkout);
  });

  it('calls logout and redirects to initial page when clicked', async () => {
    const { getByTestId } = render(
      <UserContext.Provider value={userContextMock}>
        <LogoutButton />
      </UserContext.Provider>,
      ROUTES.checkout,
    );

    const logoutButton = getByTestId('logoutButton');
    expect(logoutButton).toBeTruthy();

    await waitFor(() => fireEvent.press(logoutButton));
    expect(userContextMock.logout).toBeCalledTimes(1);
  });
});
