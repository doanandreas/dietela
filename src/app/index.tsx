import React, { FC } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeProvider } from 'react-native-elements';
import Toast from 'react-native-toast-message';

import * as ROUTES from 'constants/routes';
import { navigation } from 'constants/navigation';
import ContextProvider from 'provider';
import { theme } from 'styles/theme';

import { screenOptions, toastConfig } from './styles';
import LogoutButton from './LogoutButton';

const Stack = createStackNavigator();

const App: FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <ContextProvider>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName={ROUTES.initial}
            screenOptions={screenOptions}>
            {navigation.map((nav, i) => (
              <Stack.Screen
                key={`nav${i}`}
                name={nav.name}
                component={nav.component}
                options={{
                  title: nav.header,
                  headerShown: Boolean(nav.header),
                  headerRight: LogoutButton,
                }}
              />
            ))}
          </Stack.Navigator>
        </NavigationContainer>
        <Toast config={toastConfig} ref={Toast.setRef} />
      </ContextProvider>
    </ThemeProvider>
  );
};

export default App;
