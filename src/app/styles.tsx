import React from 'react';
import { StyleSheet } from 'react-native';
import {
  StackNavigationOptions,
  TransitionSpecs,
} from '@react-navigation/stack';
import { BaseToast, BaseToastProps } from 'react-native-toast-message';
import { colors, typographyStyles, typography } from 'styles';

export const screenOptions: StackNavigationOptions = {
  cardStyle: {
    backgroundColor: '#fff',
  },
  headerStyle: {
    backgroundColor: 'white',
    height: 62,
    elevation: 0,
  },
  headerTintColor: colors.primary,
  headerTitleStyle: typographyStyles.headingMedium,
  headerTitleAlign: 'center',
  transitionSpec: {
    open: {
      animation: 'timing',
      config: {
        duration: 1,
        delay: 0,
      },
    },
    close: TransitionSpecs.TransitionIOSSpec,
  },
};

const styles = StyleSheet.create({
  toastStyle: { borderLeftColor: 'red' },
  contentContainerStyle: { padding: 16 },
  text1Style: {
    ...typography.bodyMedium,
    color: 'red',
  },
});

export const ErrorToast = (props: BaseToastProps) => (
  <BaseToast
    {...props}
    style={styles.toastStyle}
    contentContainerStyle={styles.contentContainerStyle}
    text1Style={styles.text1Style}
    text2Style={{
      ...props.text2Style,
      ...typography.caption,
    }}
  />
);

export const toastConfig = {
  error: ErrorToast,
};
