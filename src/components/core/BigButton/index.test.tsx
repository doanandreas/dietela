import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import BigButton from '.';

describe('BigButton', () => {
  it('renders correctly when active', () => {
    render(
      <BigButton title="ima button" onPress={() => console.log('cool')} />,
    );
  });

  it('renders correctly when disabled', () => {
    render(
      <BigButton
        title="ima button"
        onPress={() => console.log('cool')}
        disabled
      />,
    );
  });

  it('renders correctly when given a testID', () => {
    render(
      <BigButton
        title="ima button"
        onPress={() => console.log('cool')}
        testID="button"
      />,
    );
  });

  it('executes onPress callback when button is pressed', async () => {
    let isPressed = false;

    const { getByText } = render(
      <BigButton
        title="ima button"
        onPress={() => (isPressed = true)}
        testID="button"
      />,
    );

    const button = getByText(/ima button/i);
    fireEvent.press(button);
    expect(isPressed).toBeTruthy();
  });
});
