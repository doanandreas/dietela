export interface Props {
  title: string;
  onPress: () => void;
  loading?: boolean;
  disabled?: true;
  testID?: string;
}
