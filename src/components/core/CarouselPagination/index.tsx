import React, { FC } from 'react';
import { Pagination } from 'react-native-snap-carousel';

import { styles } from './styles';

interface Props {
  index: number;
  length: number;
}

const CarouselPagination: FC<Props> = ({ index, length }) => (
  <Pagination
    dotsLength={length}
    activeDotIndex={index}
    dotStyle={styles.dotStyle}
    inactiveDotOpacity={0.4}
    inactiveDotScale={0.6}
  />
);

export default CarouselPagination;
