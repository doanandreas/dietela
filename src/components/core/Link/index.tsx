import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import { Button, ButtonProps } from 'react-native-elements';

import { colors } from 'styles';

interface Props extends ButtonProps {
  title: string;
  color?: string;
}

const Link: FC<Props> = ({ title, color, ...props }) => {
  const styles = stylesWithColor(color);

  return (
    <Button
      title={title}
      type="clear"
      buttonStyle={styles.buttonPadding}
      titleStyle={styles.title}
      {...props}
    />
  );
};

const stylesWithColor = (color?: string) =>
  StyleSheet.create({
    buttonPadding: {
      padding: 5,
    },
    title: {
      textDecorationLine: 'underline',
      color: color ? color : colors.buttonYellow,
    },
  });

export default Link;
