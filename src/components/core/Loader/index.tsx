import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { colors } from 'styles';
import { dietelaLogo } from 'assets/images';
import { Image } from 'react-native-elements';

const Loader = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color={colors.primary} />
  </View>
);

const DietelaCoverLoader = () => (
  <View style={styles.dietelaCover}>
    <Image
      source={dietelaLogo}
      style={styles.logo}
      placeholderStyle={styles.img}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  dietelaCover: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.primaryYellow,
  },
  logo: {
    width: 200,
    height: 100,
  },
  img: {
    backgroundColor: colors.primaryYellow,
  },
});

export default Loader;
export { DietelaCoverLoader };
