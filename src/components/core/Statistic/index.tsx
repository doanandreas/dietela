import React, { FC } from 'react';
import { View, Text } from 'react-native';
import { typographyStyles } from 'styles';
import { styles } from './styles';

interface Props {
  title: string;
  emote: string;
  content: string;
  testID?: string;
}

const Statistic: FC<Props> = ({ title, emote, content, testID }) => {
  return (
    <View testID={testID}>
      <Text style={[typographyStyles.overlineBig, styles.title]}>{title}</Text>
      <View>
        <Text style={typographyStyles.bodyMedium}>
          <View style={styles.emote}>
            <Text style={[typographyStyles.bodyMedium, styles.emote]}>
              {emote}
            </Text>
          </View>
          <View>
            <Text style={typographyStyles.bodyMedium}>{content}</Text>
          </View>
        </Text>
      </View>
    </View>
  );
};

export default Statistic;
