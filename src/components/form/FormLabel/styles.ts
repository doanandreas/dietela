import { StyleSheet } from 'react-native';
import { typography } from 'styles';

export const styles = StyleSheet.create({
  red: {
    color: 'red',
  },
  label: {
    fontWeight: '600',
    marginBottom: 4,
    ...typography.bodyMedium,
  },
});
