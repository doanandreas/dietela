import { ReactNode } from 'react';

export interface Props {
  label?: ReactNode;
  required?: boolean;
}
