import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 14,
  },
  bigSpacing: {
    marginBottom: 24,
  },
  red: {
    color: 'red',
  },
});
