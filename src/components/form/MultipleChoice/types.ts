export interface Choice {
  label: string;
  value: number;
}

export interface Props {
  questionNumber: number;
  totalQuestions: number;
  questionLabel: string;
  helperText?: string;
  errorMessage?: any;
  choices: Choice[];
  value: any;
  onChange: (_: any) => void;
}
