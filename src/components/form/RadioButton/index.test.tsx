import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import RadioButton, { RadioButtonGroup } from '.';

describe('RadioButton component', () => {
  it('renders correctly', () => {
    render(<RadioButton />);
  });
});

describe('RadioButtonGroup component', () => {
  let value: any = null;

  const props = {
    choices: [
      {
        label: 'Choice 1',
        value: 1,
      },
      {
        label: 'Choice 2',
        value: 2,
      },
    ],
    value: value,
    onChange: (v: any) => (value = v),
  };

  it('only selects one choice at a time', () => {
    const { getByText } = render(<RadioButtonGroup {...props} />);

    fireEvent.press(getByText(/Choice 1/i));
    expect(value).toEqual(1);

    fireEvent.press(getByText(/Choice 2/i));
    expect(value).toEqual(2);
  });

  it('shows error message when there is error message props', () => {
    const errorMessage = 'error';
    const { getByText } = render(
      <RadioButtonGroup {...props} errorMessage={errorMessage} />,
    );

    const errorText = getByText(errorMessage);
    expect(errorText).toBeTruthy();
  });
});
