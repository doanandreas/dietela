import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  radioButton: {
    flex: 0.48,
  },
  red: {
    color: 'red',
  },
});
