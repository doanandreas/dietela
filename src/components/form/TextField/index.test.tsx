import React from 'react';
import { render } from '@testing-library/react-native';

import TextField from '.';

describe('TextField component', () => {
  it('renders correctly', () => {
    render(<TextField />);
  });

  it('renders correctly with error message', () => {
    render(<TextField errorMessage="error" />);
  });
});
