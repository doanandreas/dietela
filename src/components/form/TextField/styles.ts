import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  red: {
    color: 'red',
  },
  smallMargin: {
    height: 10,
  },
  bigMargin: {
    height: 30,
  },
});
