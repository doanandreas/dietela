import { InputProps } from 'react-native-elements';

export interface Props extends InputProps {
  required?: boolean;
  errorMessage?: any;
}
