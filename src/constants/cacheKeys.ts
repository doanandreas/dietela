export default {
  dietProfileId: 'DIET_PROFILE_ID',
  cartId: 'CART_ID',
  authToken: 'AUTH_TOKEN',
  refreshToken: 'REFRESH_TOKEN',
  programRecommendations: 'PROGRAM_RECOMMENDATIONS',
};
