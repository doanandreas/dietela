import * as ROUTES from 'constants/routes';
import {
  AllAccessQuestionnaire,
  Checkout,
  ChoosePlan,
  ComingSoonPage,
  DietelaQuizResult,
  InitialPage,
  ManualRegistrationPage,
  LoginPage,
  ProgramDetail,
} from 'scenes';
import { FC } from 'react';

export interface NavRoute {
  name: string;
  component: FC;
  header?: string;
}

export const navigation: NavRoute[] = [
  {
    name: ROUTES.initial,
    component: InitialPage,
  },
  {
    name: ROUTES.allAccessQuestionnaire,
    component: AllAccessQuestionnaire,
    header: 'Dietela Quiz',
  },
  {
    name: ROUTES.dietelaQuizResult,
    component: DietelaQuizResult,
    header: 'Dietela Quiz Result',
  },
  {
    name: ROUTES.choosePlan,
    component: ChoosePlan,
    header: 'Choose Plan',
  },
  {
    name: ROUTES.checkout,
    component: Checkout,
    header: 'Checkout',
  },
  {
    name: ROUTES.programDetail,
    component: ProgramDetail,
    header: 'Program Dietela',
  },
  {
    name: ROUTES.nutritionistDetail,
    component: ComingSoonPage,
    header: 'Nutrisionis',
  },
  {
    name: ROUTES.registration,
    component: ManualRegistrationPage,
    header: 'Registrasi',
  },
  {
    name: ROUTES.login,
    component: LoginPage,
    header: 'Login',
  },
  {
    name: ROUTES.profile,
    component: ComingSoonPage,
    header: 'Profile',
  },
];
