export const initial = 'initial-page';
export const comingSoon = '*';

const questionnaire = 'questionnaire';
export const allAccessQuestionnaire = `${questionnaire}/all-access`;
export const dietelaQuizResult = `${questionnaire}/dietela-quiz-result`;

export const checkout = 'checkout';
export const choosePlan = `${checkout}/choose-plan`;
export const programDetail = `${checkout}/dietela-program`;
export const nutritionistDetail = `${checkout}/nutritionist`;

export const registration = 'registration';
export const login = 'login';

export const profile = 'profile';
