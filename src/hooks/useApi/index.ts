import { useState, useEffect } from 'react';
import { ApiResponse, Response } from 'services/api';

import { Toast } from 'components/core';

const useApi = <T>(
  fetchApi: () => ApiResponse<T>,
): {
  isLoading: boolean;
} & Response<T> => {
  const [isLoading, setIsLoading] = useState(false);
  const [response, setResponse] = useState({
    success: false,
  });

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      const apiResponse = await fetchApi();
      setResponse(apiResponse);
      if (!apiResponse.success) {
        Toast.show({
          type: 'error',
          text1: 'Gagal memuat data',
          text2: 'Terjadi kesalahan pada sisi kami.',
        });
      }
      setIsLoading(false);
    };

    fetchData();
  }, [fetchApi]);

  return {
    isLoading,
    ...response,
  };
};

export default useApi;
