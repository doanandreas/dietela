import { useContext, useEffect, useCallback, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { UserContext } from 'provider';
import * as ROUTES from 'constants/routes';
import CACHE_KEYS from 'constants/cacheKeys';
import { getCache } from 'utils/cache';

const useAuthEffect = (isLogin?: boolean) => {
  const { isAuthenticated, isUnpaidClient, isFirstLoading } = useContext(
    UserContext,
  );
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);

  const checkCart = useCallback(async () => {
    setIsLoading(true);
    const dietProfileId = await getCache(CACHE_KEYS.dietProfileId);
    const cartId = await getCache(CACHE_KEYS.cartId);
    if (!dietProfileId) {
      navigation.reset({
        index: 0,
        routes: [
          { name: ROUTES.initial },
          { name: ROUTES.allAccessQuestionnaire },
        ],
      });
    } else if (!cartId) {
      navigation.reset({
        index: 0,
        routes: [{ name: ROUTES.initial }, { name: ROUTES.choosePlan }],
      });
    }
    setIsLoading(false);
  }, [navigation]);

  useEffect(() => {
    if (isAuthenticated) {
      if (isUnpaidClient) {
        navigation.reset({
          index: 0,
          routes: [{ name: ROUTES.checkout }],
        });
      } else {
        navigation.reset({
          index: 0,
          routes: [{ name: ROUTES.profile }],
        });
      }
    } else if (isLogin) {
      checkCart();
    }

    return () => {
      setIsLoading(false);
    };
  }, [checkCart, isLogin, isAuthenticated, isUnpaidClient, navigation]);

  return isFirstLoading || isLoading;
};

export default useAuthEffect;
