import { useContext, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { UserContext } from 'provider';
import * as ROUTES from 'constants/routes';

const useAuthGuardEffect = (signupFallback?: boolean) => {
  const { isAuthenticated, firstAuthenticated } = useContext(UserContext);

  const navigation = useNavigation();
  useEffect(() => {
    if (!isAuthenticated && firstAuthenticated) {
      navigation.navigate(signupFallback ? ROUTES.registration : ROUTES.login);
    }
  }, [isAuthenticated, signupFallback, navigation, firstAuthenticated]);
};

export default useAuthGuardEffect;
