import { ApiResponse } from 'services/api';
import {
  LoginRequest,
  LoginResponse,
  RegistrationRequest,
  User,
  LinkUserDataResponse,
} from 'services/auth/models';

export interface iUserContext {
  user: User;
  firstAuthenticated: boolean;
  isAuthenticated: boolean;
  isUnpaidClient: boolean;
  isPaidClient: boolean;
  isNutritionist: boolean;
  isAdmin: boolean;
  isLoading: boolean;
  isFirstLoading: boolean;
  signup: (
    data: RegistrationRequest,
  ) => ApiResponse<LoginResponse | LinkUserDataResponse>;
  login: (data: LoginRequest) => ApiResponse<LoginResponse>;
  loginWithGoogle: (_?: boolean) => Promise<void>;
  logout: () => Promise<void>;
}
