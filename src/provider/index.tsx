import React, { FC } from 'react';

import { UserContext, useUserContext } from './UserContext';

const ContextProvider: FC = ({ children }) => {
  const user = useUserContext();

  return <UserContext.Provider value={user}>{children}</UserContext.Provider>;
};

export default ContextProvider;
export { UserContext };
