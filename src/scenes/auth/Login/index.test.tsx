import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';
import CACHE_KEYS from 'constants/cacheKeys';
import { setCache } from 'utils/cache';

import Login from '.';
import {
  authResponse,
  invalidLoginValues,
  validLoginValues,
} from '__mocks__/auth';
import { textField } from './schema';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Login page', () => {
  it('shows dietela cover loader when is loading', async () => {
    const { queryByText } = render(<Login />, ROUTES.login);

    await waitFor(() =>
      expect(queryByText(/Lanjut dengan Google/i)).toBeFalsy(),
    );
  });

  it('renders correctly if client has filled questionnaire and cart', async () => {
    await setCache(CACHE_KEYS.cartId, 1);
    await setCache(CACHE_KEYS.dietProfileId, 1);

    const { queryByText } = render(<Login />, ROUTES.login);

    await waitFor(() =>
      expect(queryByText(/Lanjut dengan Google/i)).toBeTruthy(),
    );
  });

  it('success when field is valid and submit success', async () => {
    const loginApi = () =>
      Promise.resolve({
        status: 201,
        data: authResponse,
      });
    mockAxios.request.mockImplementationOnce(loginApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <Login />,
      ROUTES.login,
    );

    await waitFor(() =>
      expect(queryByText(/Lanjut dengan Google/i)).toBeTruthy(),
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validLoginValues[name]);
    });

    const loginButton = getByTestId('loginButton');
    await waitFor(() => fireEvent.press(loginButton));
  });

  it('fails when field is invalid and submit success', async () => {
    const loginApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(loginApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <Login />,
      ROUTES.login,
    );

    await waitFor(() =>
      expect(queryByText(/Lanjut dengan Google/i)).toBeTruthy(),
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, invalidLoginValues[name]);
    });

    const loginButton = getByTestId('loginButton');
    await waitFor(() => fireEvent.press(loginButton));

    const toastWarning = queryByText(/Profile/i);
    expect(toastWarning).toBeFalsy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
