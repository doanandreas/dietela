import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import ManualRegistrationPage from '.';
import { textField } from './schema';
import {
  authResponse,
  validRegistrationValues,
  invalidRegistrationValues,
} from '__mocks__/auth';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ManualRegistrationPage', () => {
  it('renders correctly', () => {
    render(<ManualRegistrationPage />, ROUTES.registration);
  });

  it('success when field is valid and submit success', async () => {
    const signupApi = () =>
      Promise.resolve({
        status: 201,
        data: authResponse,
      });
    mockAxios.request.mockImplementationOnce(signupApi);

    const { getByPlaceholderText, getByTestId } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validRegistrationValues[name]);
    });

    const submitButton = getByTestId('submitButton');
    await waitFor(() => fireEvent.press(submitButton));
  });

  it('fails when field is valid and submit fails', async () => {
    const signupApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(signupApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validRegistrationValues[name]);
    });

    const submitButton = getByTestId('submitButton');
    await waitFor(() => fireEvent.press(submitButton));

    const nextPageText = queryByText(/Checkout/i);
    expect(nextPageText).toBeFalsy();
  });

  it('fails when field is invalid and submit success', async () => {
    const alreadyRegistered = 'Email is already registered';

    const signupApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: {
            name: 'Wrong name',
            email: alreadyRegistered,
            password1: 'Wrong password',
            password2: 'Wrong password',
          },
        },
      });
    mockAxios.request.mockImplementationOnce(signupApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, invalidRegistrationValues[name]);
    });

    const submitButton = getByTestId('submitButton');
    await waitFor(() => fireEvent.press(submitButton));

    const nextPageText = queryByText(/Checkout/i);
    expect(nextPageText).toBeFalsy();
  });

  test('has link button that navigates to Login Page', async () => {
    const { getByText, queryByText, queryAllByText } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );
    expect(queryByText(/Login disini/i)).toBeTruthy();
    await waitFor(() => fireEvent.press(getByText(/Login disini/i)));

    expect(queryAllByText(/Login/i)).toBeTruthy();
  });
});
