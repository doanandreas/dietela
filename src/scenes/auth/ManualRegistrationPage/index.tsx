import React, { FC, useContext } from 'react';
import { useAuthEffect, useForm } from 'hooks';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/core';

import { BigButton, Link, Toast } from 'components/core';
import { Section } from 'components/layout';
import { TextField } from 'components/form';
import { GoogleLoginButton } from '../components';

import { fieldValidation, initialValues, textField } from './schema';
import { generateValidationSchema } from 'utils/form';
import { UserContext } from 'provider';
import * as ROUTES from 'constants/routes';

import { layoutStyles } from 'styles';

const isPasswordField = (name: string) =>
  name === 'password1' || name === 'password2';

const ManualRegistrationPage: FC = () => {
  const { signup, loginWithGoogle, isLoading } = useContext(UserContext);
  const navigation = useNavigation();

  const {
    getTextInputProps,
    handleSubmit,
    isSubmitting,
    setFieldError,
  } = useForm({
    initialValues,
    validationSchema: generateValidationSchema(fieldValidation),
    onSubmit: async (values) => {
      const response = await signup(values);

      if (!response.success) {
        setFieldError('name', response.error.name);
        setFieldError('email', response.error.email);
        setFieldError('password1', response.error.password1);
        setFieldError('password2', response.error.password2);

        Toast.show({
          type: 'error',
          text1: 'Gagal registrasi akun',
          text2: 'Terjadi kesalahan registrasi. Silakan coba lagi',
        });
      }
    },
  });

  const signupWithGoogle = () => loginWithGoogle(false);

  useAuthEffect();

  return (
    <ScrollView contentContainerStyle={layoutStyles}>
      {textField.map((fieldProps, i) => (
        <TextField
          key={`field${i}`}
          label={fieldProps.label}
          required={fieldProps.required}
          placeholder={fieldProps.placeholder}
          {...getTextInputProps(fieldProps.name)}
          secureTextEntry={isPasswordField(fieldProps.name)}
        />
      ))}
      <BigButton
        title="daftarkan akun"
        onPress={handleSubmit}
        loading={isSubmitting}
        testID="submitButton"
      />
      <Section>
        <GoogleLoginButton onPress={signupWithGoogle} isLoading={isLoading} />
      </Section>
      <Section>
        <Link
          title="Sudah punya akun? Login disini"
          onPress={() => navigation.navigate(ROUTES.login)}
        />
      </Section>
    </ScrollView>
  );
};

export default ManualRegistrationPage;
