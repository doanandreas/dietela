import React, { FC } from 'react';
import { Button, Image } from 'react-native-elements';
import { googleLogo } from 'assets/images';

import { styles } from './styles';
import { Props } from './types';

const GoogleLoginButton: FC<Props> = ({ onPress, isLoading }) => (
  <Button
    title="Lanjut dengan Google"
    icon={<Image source={googleLogo} style={styles.googleIcon} />}
    onPress={onPress}
    containerStyle={styles.container}
    buttonStyle={styles.button}
    titleStyle={styles.title}
    disabled={isLoading}
  />
);

export default GoogleLoginButton;
