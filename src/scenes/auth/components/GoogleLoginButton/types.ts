export interface Props {
  onPress: () => void;
  isLoading: boolean;
}
