import React, { FC } from 'react';
import { View } from 'react-native';

import { Text, Button } from 'react-native-elements';

import { Props } from './types';
import { styles } from './styles';
import { typographyStyles, colors } from 'styles';

const CheckoutCard: FC<Props> = ({ content, type, onReadMore }) => (
  <View style={styles.container}>
    <Text style={typographyStyles.overlineBig}>
      Pilihan {type === 'program' ? 'Program' : 'Nutrisionis'} Anda
    </Text>
    <Text style={styles.title}>
      {type === 'program' ? '📝  ' : '👩🏻‍⚕️  '}
      {content}
    </Text>
    <Button
      title="Baca selengkapnya"
      type="clear"
      icon={{
        name: 'arrow-forward',
        size: 22,
        color: colors.primary,
      }}
      onPress={onReadMore}
      iconRight
      titleStyle={styles.readMore}
      buttonStyle={styles.buttonStyle}
    />
  </View>
);

export default CheckoutCard;
