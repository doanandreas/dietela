import React from 'react';
import { withAuthRender, waitFor, fireEvent } from 'utils/testing';
import axios from 'axios';

import Checkout from '.';

import * as ROUTES from 'constants/routes';
import CACHE_KEYS from 'constants/cacheKeys';
import { DietelaProgram } from 'services/dietelaQuiz/quizResult';
import { setCache } from 'utils/cache';
import { mockProgramRecommendations } from '__mocks__/quizResult';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Checkout', () => {
  const nutritionist = {
    id: 1,
    full_name_and_degree: 'Wendy',
    registration_certificate_no: '123',
    university: 'UI',
    mastered_nutritional_problems: 'diet',
    handled_age_group: '18',
    another_practice_place: '-',
    languages: 'English',
  };
  const retrieveCartApi = () =>
    Promise.resolve({
      status: 200,
      data: {
        id: 1,
        program: {
          id: 1,
          unique_code: DietelaProgram.TRIAL,
        },
        nutritionist,
      },
    });

  it('redirects to program detail page when user clicks Baca selengkapnya button for program', async () => {
    await setCache(CACHE_KEYS.cartId, 1);
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getAllByText, getByText } = withAuthRender(
      <Checkout />,
      ROUTES.checkout,
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const chosenProgram = getByText(/One Week Trial/i);
    expect(chosenProgram).toBeTruthy();

    const readMoreButton = getAllByText(/Baca selengkapnya/i)[0];
    expect(readMoreButton).toBeTruthy();
    fireEvent.press(readMoreButton);

    const programDetailPage = getByText(/Program Dietela/i);
    expect(programDetailPage).toBeTruthy();
  });

  it('redirects to nutritionist detail page when user clicks Baca selengkapnya button for nutritionist', async () => {
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getAllByText, getByText } = withAuthRender(
      <Checkout />,
      ROUTES.checkout,
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const chosenNutritionist = getByText(/Wendy/i);
    expect(chosenNutritionist).toBeTruthy();

    const readMoreButton = getAllByText(/Baca selengkapnya/i)[1];
    expect(readMoreButton).toBeTruthy();
    fireEvent.press(readMoreButton);

    const nutritionistDetailPage = getByText(/Coming Soon/i);
    expect(nutritionistDetailPage).toBeTruthy();
  });

  it('redirects to choose plan page when user clicks Ganti Pilihan button', async () => {
    await setCache(
      CACHE_KEYS.programRecommendations,
      JSON.stringify(mockProgramRecommendations),
    );
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getByText } = withAuthRender(<Checkout />, ROUTES.checkout);
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const changePlanButton = getByText(/ganti pilihan/i);
    expect(changePlanButton).toBeTruthy();
    await waitFor(() => fireEvent.press(changePlanButton));

    const choosePlanPage = getByText(/Choose Plan/i);
    expect(choosePlanPage).toBeTruthy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
