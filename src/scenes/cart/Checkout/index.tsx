import React, { FC, useCallback } from 'react';
import { View } from 'react-native';
import { Text, Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

import { Loader, BigButton } from 'components/core';
import { Section } from 'components/layout';
import CACHE_KEYS from 'constants/cacheKeys';
import * as ROUTES from 'constants/routes';
import { dietPrograms } from 'constants/dietelaProgram';
import { useApi, useAuthGuardEffect } from 'hooks';
import { retrieveCartApi } from 'services/payment';
import { getCache } from 'utils/cache';
import { typographyStyles } from 'styles';

import { styles } from './styles';
import { CheckoutCard } from './components';

const Checkout: FC = () => {
  const navigation = useNavigation();

  const fetchCart = useCallback(async () => {
    const cartId = await getCache(CACHE_KEYS.cartId);
    return await retrieveCartApi(cartId);
  }, []);

  const { isLoading, data } = useApi(fetchCart);

  useAuthGuardEffect(true);

  if (isLoading) {
    return <Loader />;
  }
  return (
    <View style={styles.container}>
      <View>
        <CheckoutCard
          content={data ? dietPrograms[data.program.unique_code].title : '-'}
          type="program"
          onReadMore={() =>
            navigation.navigate(ROUTES.programDetail, {
              id: data?.program.unique_code,
            })
          }
        />
        <CheckoutCard
          content={data ? data.nutritionist.full_name_and_degree : '-'}
          type="nutritionist"
          onReadMore={() =>
            navigation.navigate(ROUTES.nutritionistDetail, {
              id: data?.nutritionist.id,
            })
          }
        />
      </View>
      <View style={styles.priceContainer}>
        <Text style={typographyStyles.headingMedium}>Harga:</Text>
        {data ? (
          <View style={styles.currencyContainer}>
            <Text style={styles.currency}>Rp</Text>
            <Text style={styles.basePrice}>
              {dietPrograms[data.program.unique_code].price}
            </Text>
          </View>
        ) : (
          <Text>-</Text>
        )}
      </View>
      <View>
        <Button
          title="ganti pilihan"
          type="outline"
          onPress={() => navigation.navigate(ROUTES.choosePlan)}
          buttonStyle={styles.buttonStyle}
          titleStyle={[typographyStyles.overlineBig, styles.titleStyle]}
        />
        <Section>
          <BigButton title="bayar dengan midtrans" onPress={console.log} />
        </Section>
      </View>
    </View>
  );
};

export default Checkout;
