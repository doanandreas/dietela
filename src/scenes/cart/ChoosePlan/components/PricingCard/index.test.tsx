import React from 'react';
import { render } from '@testing-library/react-native';

import PricingCard from '.';

describe('PricingCard component', () => {
  it('renders correctly', () => {
    render(<PricingCard onReadMore={jest.fn()} />);
  });

  it('shows Terpilih button when selected', () => {
    const { getByText } = render(
      <PricingCard onReadMore={jest.fn()} isSelected />,
    );
    expect(getByText(/Terpilih/i)).toBeTruthy();
  });

  it('shows price if price props is provided', () => {
    const { getByText } = render(
      <PricingCard onReadMore={jest.fn()} price="123456" />,
    );
    expect(getByText(/123456/i)).toBeTruthy();
  });

  it('shows info list if info props is provided', () => {
    const { getByText } = render(
      <PricingCard onReadMore={jest.fn()} info={['very good']} />,
    );
    expect(getByText(/very good/i)).toBeTruthy();
  });
});
