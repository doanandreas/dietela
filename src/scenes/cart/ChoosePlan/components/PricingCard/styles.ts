import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: colors.border,
    borderRadius: 6,
    padding: 20,
    paddingBottom: 0,
    marginTop: 20,
  },
  selected: {
    borderColor: colors.secondaryVariant,
  },
  title: {
    ...typography.headingMedium,
    marginBottom: 4,
  },
  basePriceContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
  },
  currency: {
    ...typography.bodySmall,
    color: colors.formLabel,
    marginRight: 6,
    marginTop: 12,
  },
  basePrice: {
    ...typography.displayMediumMontserrat,
    fontSize: 40,
    color: colors.primary,
  },
  buttonContainer: {
    marginTop: 10,
  },
  buttonStyle: {
    backgroundColor: colors.secondaryVariant,
  },
  selectedButton: {
    backgroundColor: colors.primaryVariant,
  },
  titleStyle: {
    color: colors.textBlack,
  },
  readMore: {
    color: colors.primaryVariant,
  },
  info: {
    paddingVertical: 4,
  },
});
