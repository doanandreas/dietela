import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import PricingList from '.';

describe('PricingList component', () => {
  let value: any = null;

  const props = {
    headerText: 'hello',
    items: [
      {
        title: 'Choice 1',
        price: '800000',
        value: 'the_choice',
        onReadMore: jest.fn(),
      },
      {
        title: 'Choice 2',
        price: '800000',
        value: 'the_choice_2',
        onReadMore: jest.fn(),
      },
    ],
    value,
    onChange: (v: any) => (value = v),
  };

  it('only selects one pricing card at a time', () => {
    const { getByText } = render(<PricingList {...props} />);

    fireEvent.press(getByText(/Pilih Choice 1/i));
    expect(value).toEqual('the_choice');

    fireEvent.press(getByText(/Pilih Choice 2/i));
    expect(value).toEqual('the_choice_2');
  });
});
