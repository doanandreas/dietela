import React, { FC } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-elements';

import PricingCard from '../PricingCard';
import { Props } from './types';
import { typographyStyles } from 'styles';

const PricingList: FC<Props> = ({ headerText, items, value, onChange }) => (
  <View>
    <Text style={typographyStyles.headingMedium}>{headerText}</Text>
    {items.map((item) => (
      <PricingCard
        {...item}
        onButtonPress={() => onChange(item.value)}
        isSelected={value === item.value}
        key={item.value}
      />
    ))}
  </View>
);

export default PricingList;
