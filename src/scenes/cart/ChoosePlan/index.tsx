import React, { FC, useState, useCallback, useEffect } from 'react';
import { ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { WizardContainer, Loader, Toast } from 'components/core';
import CACHE_KEYS from 'constants/cacheKeys';
import * as ROUTES from 'constants/routes';
import { useApi } from 'hooks';
import {
  dietPrograms,
  defaultProgramRecommendations,
} from 'constants/dietelaProgram';
import { retrieveNutritionistsApi } from 'services/nutritionists';
import { Nutritionist } from 'services/nutritionists/models';
import { createCartApi } from 'services/payment';
import { layoutStyles } from 'styles';
import { setCache, getCache } from 'utils/cache';

import { PricingList } from './components';
import { initialValues, getRecommendedPrograms } from './schema';

const ChoosePlan: FC = () => {
  const navigation = useNavigation();

  const [currentPage, setCurrentPage] = useState(1);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [values, setValues] = useState(initialValues);
  const [programs, setPrograms] = useState(undefined);

  const handleSubmit = async () => {
    setIsSubmitting(true);
    const response = await createCartApi(values);
    setIsSubmitting(false);

    if (response.success) {
      await setCache(CACHE_KEYS.cartId, response.data?.id);
      navigation.navigate(ROUTES.checkout, response.data);
    } else {
      Toast.show({
        type: 'error',
        text1: 'Gagal menyimpan data',
        text2: 'Terjadi kesalahan pada sisi kami. Silakan coba lagi',
      });
    }
  };

  const handleChange = (name: string, value: string) => {
    setValues({
      ...values,
      [name]: value,
    });
  };

  const isCurrentPageError = (): boolean => {
    const fieldName = currentPage === 1 ? 'program' : 'nutritionist';
    return values[fieldName] === null;
  };

  const getPrograms = useCallback(async () => {
    const cachedPrograms = await getCache(CACHE_KEYS.programRecommendations);
    setPrograms(
      cachedPrograms
        ? JSON.parse(cachedPrograms)
        : defaultProgramRecommendations,
    );
  }, []);

  useEffect(() => {
    getPrograms();
  }, [getPrograms]);

  const { isLoading, data: nutritionists = [] } = useApi(
    retrieveNutritionistsApi,
  );

  if (isLoading || programs === undefined) {
    return <Loader />;
  }
  return (
    <WizardContainer
      currentStep={currentPage}
      setCurrentStep={setCurrentPage}
      onFinish={handleSubmit}
      finishButtonLabel="Bayar"
      isNextDisabled={isCurrentPageError()}
      isLoading={isSubmitting}
      components={[
        <ScrollView contentContainerStyle={layoutStyles}>
          <PricingList
            headerText="Pilih Rekomendasi Program Dietela"
            items={getRecommendedPrograms(programs).map((code) => ({
              ...dietPrograms[code],
              value: code,
              onReadMore: () =>
                navigation.navigate(ROUTES.programDetail, { id: code }),
            }))}
            value={values.program}
            onChange={(v) => handleChange('program', v)}
          />
        </ScrollView>,
        <ScrollView contentContainerStyle={layoutStyles}>
          <PricingList
            headerText="Pilih Nutrisionis Anda"
            items={nutritionists.map((nutritionist: Nutritionist) => ({
              title: nutritionist.full_name_and_degree,
              value: nutritionist.id,
              info: nutritionist.mastered_nutritional_problems
                .split(';')
                .slice(0, 3),
              onReadMore: () =>
                navigation.navigate(ROUTES.nutritionistDetail, {
                  id: nutritionist.id,
                }),
            }))}
            value={values.nutritionist}
            onChange={(v) => handleChange('nutritionist', v)}
          />
        </ScrollView>,
      ]}
    />
  );
};

export default ChoosePlan;
