import {
  ProgramRecommendations,
  DietelaProgram,
} from 'services/dietelaQuiz/quizResult';
import { CartRequest } from 'services/payment/models';

export const initialValues: CartRequest = {
  program: null,
  nutritionist: null,
};

export const getRecommendedPrograms = (
  programs?: ProgramRecommendations,
): DietelaProgram[] =>
  programs ? Object.values(programs).filter((item) => item) : [];
