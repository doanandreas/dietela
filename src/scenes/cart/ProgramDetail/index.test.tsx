import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import ProgramDetail from '.';
import { dietPrograms } from 'constants/dietelaProgram';
import { DietelaProgram } from 'services/dietelaQuiz/quizResult';

describe('ProgramDetail', () => {
  const id = DietelaProgram.GOALS_1;

  it('shows program details content correctly', () => {
    const { getByText } = render(<ProgramDetail />, ROUTES.programDetail, {
      id: id,
    });
    const programDetail = dietPrograms[id].details;

    const description = getByText(programDetail.description);
    expect(description).toBeTruthy();

    const firstDetail = programDetail.details[0];
    expect(getByText(firstDetail.title)).toBeTruthy();

    const secondDetail = programDetail.details[1];
    expect(getByText(secondDetail.title)).toBeTruthy();
  });
});
