import React from 'react';
import { render } from '@testing-library/react-native';

import ComingSoonPage from '.';

describe('ComingSoonPage', () => {
  it('renders correctly', () => {
    const { getByText } = render(<ComingSoonPage />);
    expect(getByText(/Coming Soon/i)).toBeTruthy();
  });
});
