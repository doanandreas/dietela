import React, { FC } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const ComingSoonPage: FC = () => (
  <View style={styles.container}>
    <Text>Coming Soon</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ComingSoonPage;
