import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';

import * as ROUTES from 'constants/routes';
import InitialPage from '.';

describe('InitialPage', () => {
  test('shows dietela cover loader when is loading', async () => {
    const { queryByTestId } = render(<InitialPage />, ROUTES.initial, {
      isFirstLoading: true,
    });

    expect(queryByTestId('background')).toBeFalsy();
  });

  test('has background image', () => {
    const { queryByTestId } = render(<InitialPage />, ROUTES.initial);

    expect(queryByTestId('background')).toBeTruthy();
  });

  test('has Dietela logo', () => {
    const { queryByTestId } = render(<InitialPage />, ROUTES.initial);

    expect(queryByTestId('logo')).toBeTruthy();
  });

  test('has call-to-action button that navigates to Dietela Quiz', () => {
    const { getByText, queryByText } = render(<InitialPage />, ROUTES.initial);
    expect(queryByText(/konsultasi sekarang/i)).toBeTruthy();
    fireEvent.press(getByText(/konsultasi sekarang/i));

    expect(queryByText(/Dietela Quiz/i)).toBeTruthy();
  });

  test('has link button that navigates to Login Page', async () => {
    const { getByText, queryByText, queryAllByText } = render(
      <InitialPage />,
      ROUTES.initial,
    );
    expect(queryByText(/Login disini/i)).toBeTruthy();
    await waitFor(() => fireEvent.press(getByText(/Login disini/i)));

    expect(queryAllByText(/Login/i)).toBeTruthy();
  });
});
