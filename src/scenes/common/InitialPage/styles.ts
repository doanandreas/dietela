import { Dimensions, StyleSheet } from 'react-native';
import { colors } from 'styles';

let { height } = Dimensions.get('window');

export const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'space-between',
  },
  bgImage: {
    width: '100%',
    height: '100%',
  },
  logo: {
    marginTop: height * 0.05,
    height: 100,
    resizeMode: 'contain',
    overflow: 'visible',
  },
  ctaContainer: {
    marginBottom: height * 0.05,
  },
  headingContainer: {
    marginBottom: height * 0.05,
  },
  heading: {
    marginBottom: 5,
    color: colors.neutralLight,
  },
});
