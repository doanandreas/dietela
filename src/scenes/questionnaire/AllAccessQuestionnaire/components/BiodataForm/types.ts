import { Props as TextFieldProps } from 'components/form/TextField/types';
import { RadioButtonGroupProps } from 'components/form/RadioButton/types';

export interface Props {
  textFields: TextFieldProps[];
  radioButtonGroups: RadioButtonGroupProps[];
}
