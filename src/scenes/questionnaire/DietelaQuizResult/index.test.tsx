import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import DietelaQuizResult from '.';
import { mockQuizResult } from '__mocks__/quizResult';

describe('DietelaQuizResult', () => {
  it('renders correctly', () => {
    render(<DietelaQuizResult />, ROUTES.dietelaQuizResult, mockQuizResult);
  });
});
