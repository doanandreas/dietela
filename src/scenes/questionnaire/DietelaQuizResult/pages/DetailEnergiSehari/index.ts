import { ResultPageContent } from '../types';

const detailEnergiSehari = (): ResultPageContent => ({
  title: 'Aktivitas fisik',
  mainInfo: {
    infos: [
      {
        label: 'energi makan pagi',
        content: '🔒 Terkunci',
      },
      {
        label: 'energi snack pagi',
        content: '🔒 Terkunci',
      },
      {
        label: 'energi makan siang',
        content: '🔒 Terkunci',
      },
      {
        label: 'energi snack siang',
        content: '🔒 Terkunci',
      },
      {
        label: 'energi makan malam',
        content: '🔒 Terkunci',
      },
    ],
    status: 'healthy',
  },
  sections: [
    {
      header: 'Ini rekomendasi yang Dietela berikan untukmu:',
      content: {
        textCard: [
          'Dengan Program Dietela, tidak hanya kamu bisa mengetahui kebutuhan energi serta gizimu secara detail, kamu akan mendapatkan diet yang dipersonalisasi sesuai gayamu, lho! Klik tombol dibawah untuk detail lebih lanjut.',
        ],
      },
    },
  ],
});

export default detailEnergiSehari;
