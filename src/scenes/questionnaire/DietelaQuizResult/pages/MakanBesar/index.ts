import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { makanBesarStatus } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';
import { genericResult, getAnswerString, getInfos } from '../utils';

const makanBesar = (response: DietProfileResponse): ResultPageContent => {
  const result = response.quiz_result;
  const recommendation = result.large_meal_diet_recommendation;
  const { infosContent, infoStatus } = getInfos(
    makanBesarStatus[recommendation],
  );

  const meal = getAnswerString(
    'large_meal_in_one_day',
    response.large_meal_in_one_day,
  );

  return genericResult(
    'Frekuensi Makan Besar dalam Sehari',
    'status makan besar',
    infosContent,
    infoStatus,
    recommendation,
    [meal],
  );
};

export default makanBesar;
