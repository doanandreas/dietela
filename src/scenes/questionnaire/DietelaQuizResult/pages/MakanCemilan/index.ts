import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { cemilanStatus } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';
import { genericResult, getAnswerString, getInfos } from '../utils';

const makanCemilan = (response: DietProfileResponse): ResultPageContent => {
  const result = response.quiz_result;
  const recommendation = result.snacks_diet_recommendation;
  const { infosContent, infoStatus } = getInfos(cemilanStatus[recommendation]);

  // snacks_in_one_day
  const snack = getAnswerString(
    'snacks_in_one_day',
    response.snacks_in_one_day,
  );

  return genericResult(
    'Frekuensi Makan Cemilan dalam Sehari',
    'status makan cemilan',
    infosContent,
    infoStatus,
    recommendation,
    [snack],
  );
};

export default makanCemilan;
