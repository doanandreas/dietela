import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { sarapanStatus } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';
import { genericResult, getAnswerString, getInfos } from '../utils';

const makanPagi = (response: DietProfileResponse): ResultPageContent => {
  const result = response.quiz_result;
  const recommendation = result.breakfast_recommendation;
  const { infosContent, infoStatus } = getInfos(sarapanStatus[recommendation]);

  // breakfast_type
  const breakfast = getAnswerString('breakfast_type', response.breakfast_type);

  return genericResult(
    'Porsi Makan Pagi',
    'status makan pagi',
    infosContent,
    infoStatus,
    recommendation,
    [breakfast],
  );
};

export default makanPagi;
