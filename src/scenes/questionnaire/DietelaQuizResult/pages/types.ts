export interface ResultPageContent {
  title: string;
  mainInfo: {
    infos: {
      label: string;
      content: string;
    }[];
    status: 'healthy' | 'warning' | 'danger';
  };
  sections: {
    header: string;
    content: {
      statistics?: {
        label: string;
        emote: string;
        content: string;
      }[][];
      textCard?: string[];
    };
  }[];
}
