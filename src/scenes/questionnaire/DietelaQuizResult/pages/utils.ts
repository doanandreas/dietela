import { allAccessQuestions } from 'scenes/questionnaire/AllAccessQuestionnaire/schema';
import { ResultPageContent } from './types';

export const genericResult = (
  title,
  infosLabel,
  infosContent,
  infoStatus,
  recommendation,
  questions,
): ResultPageContent => ({
  title: title,
  mainInfo: {
    infos: [
      {
        label: infosLabel,
        content: infosContent,
      },
    ],
    status: infoStatus,
  },
  sections: [
    {
      header: 'Ini rekomendasi yang Dietela berikan untukmu:',
      content: {
        textCard: [recommendation],
      },
    },
    {
      header: 'Berdasarkan jawaban kuis kamu:',
      content: {
        textCard: questions,
      },
    },
  ],
});

export const getInfos = (infos) => ({
  infosContent: infos.headline,
  infoStatus: infos.status,
});

export const getAnswerString = (question: string, choice: number) => {
  return allAccessQuestions.find((qst) => qst.fieldName === question)
    ?.choiceList[choice - 1];
};
