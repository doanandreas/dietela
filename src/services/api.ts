import axios, { AxiosRequestConfig } from 'axios';

export enum RequestMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

export interface Response<T> {
  success: boolean;
  data?: T;
  error?: any;
}

export type ApiResponse<T> = Promise<Response<T>>;

const apiInstance = axios.create({
  baseURL: 'https://dietela-backend.herokuapp.com/',
});

export async function api<T>(
  method: RequestMethod = RequestMethod.GET,
  url: string,
  body: object = {},
  headers: object = {},
): ApiResponse<T> {
  const requestData: AxiosRequestConfig = {
    url,
    method,
    data: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      ...headers,
    },
  };

  return await apiInstance
    .request(requestData)
    .then((res) => ({
      success: true,
      data: res.data,
    }))
    .catch((err) => ({
      success: false,
      error: err.response.data,
    }));
}

export const setAuthHeader = (token: string): void => {
  apiInstance.defaults.headers.Authorization = `Bearer ${token}`;
};

export const resetAuthHeader = (): void => {
  apiInstance.defaults.headers.Authorization = '';
};

export const set401Callback = (callback: () => void): void => {
  apiInstance.interceptors.response.use(undefined, (err) => {
    if (err.response.status === 401) {
      callback();
    }
    return Promise.reject(err);
  });
};
