import {
  BodyMassConstants,
  BreakfastReponse,
  LargeMealDietRecommendation,
  PhysicalActivityResponse,
  SnacksDietRecommendation,
  SugarSaltFatProblemResponse,
  VegetableAndFruitSufficiencyResponse,
  ProgramRecommendations,
} from './quizResult';

export interface DietProfileRequest {
  name: string;
  email: string;
  age: number;
  weight: number;
  height: number;
  gender: number;
  special_condition: number;
  body_activity: number;
  vegetables_in_one_day: number;
  fruits_in_one_day: number;
  fried_food_in_one_day: number;
  sweet_snacks_in_one_day: number;
  sweet_drinks_in_one_day: number;
  packaged_food_in_one_day: number;
  large_meal_in_one_day: number;
  snacks_in_one_day: number;
  breakfast_type: number;
  current_condition: number;
  problem_to_solve: number;
  health_problem: number[];
}

export interface QuizResult {
  diet_profile: number;
  age: number;
  weight: number;
  height: number;
  gender: number;
  body_mass_index: number;
  nutrition_status: BodyMassConstants;
  ideal_weight_range: {
    max: number;
    min: number;
  };
  daily_energy_needs: number;
  daily_nutrition_needs: {
    fat_needs: number;
    fiber_needs: number;
    protein_needs: number;
    carbohydrate_needs: number;
  };
  vegetable_and_fruit_sufficiency: VegetableAndFruitSufficiencyResponse;
  vegetable_and_fruit_diet_recommendation: string;
  sugar_salt_fat_problem: SugarSaltFatProblemResponse;
  sugar_salt_fat_diet_recommendation: string;
  large_meal_diet_recommendation: LargeMealDietRecommendation;
  snacks_diet_recommendation: SnacksDietRecommendation;
  breakfast_recommendation: BreakfastReponse;
  energy_needed_per_dine: {
    lunch: number;
    dinner: number;
    breakfast: number;
    morning_snack: number;
    afternoon_snack: number;
  };
  physical_activity_recommendation: PhysicalActivityResponse;
  program_recommendation: ProgramRecommendations;
}

export interface DietProfileResponse extends DietProfileRequest {
  id: number;
  quiz_result: QuizResult;
}
