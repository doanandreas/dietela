import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import { Nutritionist } from './models';

export const retrieveNutritionistsApi = (): ApiResponse<Nutritionist[]> => {
  return api(RequestMethod.GET, apiUrls.nutritionists);
};
