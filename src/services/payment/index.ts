import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import { CartRequest, CartResponse } from './models';

export const createCartApi = (body: CartRequest): ApiResponse<CartResponse> => {
  return api(RequestMethod.POST, apiUrls.cart, body);
};

export const retrieveCartApi = (
  id: string | null,
): ApiResponse<CartResponse> => {
  if (id) {
    return api(RequestMethod.GET, apiUrls.cartById(id));
  }
  return Promise.reject('Anda belum memilih program diet.');
};
