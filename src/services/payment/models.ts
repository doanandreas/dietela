import { DietelaProgram } from 'services/dietelaQuiz/quizResult';
import { Nutritionist } from 'services/nutritionists/models';

export interface CartRequest {
  program: DietelaProgram | null;
  nutritionist: number | null;
}

export interface CartResponse {
  id: number;
  program: {
    id: number;
    name: string;
    price: number;
    unique_code: DietelaProgram;
  };
  nutritionist: Nutritionist;
}
