const payment = 'payment/';

export const cart = `${payment}cart/`;
export const cartById = (id: string) => `${payment}cart/${id}/`;
