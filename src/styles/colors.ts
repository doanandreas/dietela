export const colors = {
  primary: '#168C80',
  primaryVariant: '#14A68B',
  primaryYellow: '#F8E3A2',
  secondary: '#F2E52E',
  secondaryVariant: '#F2CD13',
  neutral: '#C2CBE2',
  neutralLight: '#F6F8FA',
  formLabel: '#666666',
  border: '#E0E0E0',
  buttonYellow: '#E6AF05',
  textBlack: '#000000',
};
