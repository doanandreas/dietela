import { Props as TextFieldProps } from 'components/form/TextField/types';
import { Props as FormLabelProps } from 'components/form/FormLabel/types';
import { Choice } from 'components/form/MultipleChoice/types';

export type TextFieldSchema = TextFieldProps & {
  name: string;
};

export interface RadioButtonGroupSchema extends FormLabelProps {
  choices: Choice[];
  name: string;
}
