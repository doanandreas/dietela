import React, { FC, ReactElement } from 'react';
import { render, RenderOptions } from '@testing-library/react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { navigation } from 'constants/navigation';
import { UserContext } from 'provider';
import { UserRole } from 'services/auth/models';

const Stack = createStackNavigator();

interface TestProvider {
  route: string;
  params?: any;
  isAuth?: boolean;
  children: ReactElement;
}

const TestProvider: FC<TestProvider> = ({
  route,
  params,
  isAuth,
  children,
}) => {
  const user = isAuth
    ? {
        id: 1,
        email: 'user.test@gmail.com',
        name: 'User Test',
        role: UserRole.CLIENT,
      }
    : {
        id: null,
        email: '',
        name: '',
        role: null,
      };

  const mockFn = (_?: any) => Promise.reject();

  return (
    <UserContext.Provider
      value={{
        user,
        isAuthenticated: Boolean(isAuth),
        isUnpaidClient: false,
        isPaidClient: false,
        isNutritionist: false,
        isAdmin: false,
        isLoading: false,
        isFirstLoading: Boolean(params ? params.isFirstLoading : false),
        signup: mockFn,
        login: mockFn,
        loginWithGoogle: mockFn,
        logout: mockFn,
      }}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={route}>
          {navigation.map((nav, i) => (
            <Stack.Screen
              key={`nav${i}`}
              name={nav.name}
              component={route === nav.name ? () => children : nav.component}
              initialParams={params}
              options={{
                title: nav.header,
                headerShown: Boolean(nav.header),
              }}
            />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    </UserContext.Provider>
  );
};

const customRender = (
  ui: ReactElement,
  route: string,
  params?: any,
  options?: Omit<RenderOptions, 'queries'>,
) =>
  render(ui, {
    wrapper: () => (
      <TestProvider route={route} params={params}>
        {ui}
      </TestProvider>
    ),
    ...options,
  });

const withAuthRender = (
  ui: ReactElement,
  route: string,
  params?: any,
  options?: Omit<RenderOptions, 'queries'>,
) =>
  render(ui, {
    wrapper: () => (
      <TestProvider route={route} params={params} isAuth>
        {ui}
      </TestProvider>
    ),
    ...options,
  });

export * from '@testing-library/react-native';
export { customRender as render };
export { withAuthRender };
